# Dev TypeScript #

### TypeScript Adbee Meetup Examples ###

* TypeScript data types, classes etc.
* 1.0.0

### How do I get set up? ###

* Install gulp cli `npm install -g gulp-cli`
* Install typescript compiler `npm install -g typescript`
* Install packages `npm install`
* Run `gulp`
* Open dist/index.html in browser

### Who do I talk to? ###

* michal.ustanik@adbee.sk