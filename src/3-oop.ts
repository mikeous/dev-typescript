interface ClockInterface {
    currentTime: Date;
    setTime(d: Date);
}

abstract class Clock implements ClockInterface {
    private h: number;
    private m: number;

    constructor(h: number, m: number) {
        this.h = h;
        this.m = m;
    }

    public printTime(): string {
        return this.h + ':' + this.m;
    }
}

export class DigitalClock extends Clock{
    private readonly digital: number;
    private _second: number;

    constructor(h: number, m: number) {
        super(h, m);
    }

    public get second(): number {
        return this._second;
    }

    public set second(second: number) {
        this._second = second;
    }
}

