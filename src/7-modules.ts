

/// jquery typings
// jquery.d.ts
// declare let $: JQuery;
// export default $;


// app.js
// import $ from "JQuery";
// $("button.continue").html( "Next Step..." );

// --- one namespace accross multiple files
namespace Validation {
    export interface StringValidator {
        isAcceptable(s: string): boolean;
    }
}

// other file
/// <reference path="Validation.ts" />
namespace Validation {
    const lettersRegexp = /^[A-Za-z]+$/;
    export class LettersOnlyValidator implements StringValidator {
        isAcceptable(s: string) {
            return lettersRegexp.test(s);
        }
    }
}
