// optional params + rest params
const voidFn = function(optionalParam?: string, ...restOfName: string[]): void {
    console.log('doesnt return');
}

const types = function(): void {

    let isDone: boolean = false;
    let decimal: number = 6;
    let hex: number = 0xf00d;
    let binary: number = 0b1010;
    let octal: number = 0o744;

    // double quotes
    let color: string = "blue";

    // or single
    color = 'red';

    let list: number[] = [1, 2, 3];


    // Declare a tuple type
    let x: [string, number];
    // Initialize it
    x = ["hello", 10]; // OK
    // Initialize it incorrectly
    // x = [10, "hello"]; // Error

    // Enum
    enum Color {
        Red = 1,
        Green,
        Blue
    }
    let c: Color = Color.Green;

    let notSure: any = 4;
    notSure = "maybe a string instead";
    notSure = false; // okay, definitely a boolean

    // void with optional params
    voidFn();

    // Not much else we can assign to these variables!
    let u: undefined = undefined;
    let n: null = null;

    // never wtf? :D
    // Function returning never must have unreachable end point
    function error(message: string): never {
        throw new Error(message);
    }

    // Inferred return type is never
    function fail() {
        return error("Something failed");
    }

    // Function returning never must have unreachable end point
    function infiniteLoop(): never {
        while (true) {
            console.log('err');
        }
    }
}

export { types };