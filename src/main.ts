// 1.
// import { sayHello } from "./1-hello-world";
// console.log(sayHello("Adbee"));


// 2.
// import { types } from "./2-types";
// types();


// 3.
import { DigitalClock } from "./3-oop";
let digitalClock = new DigitalClock(14, 35);
console.log(digitalClock.printTime());
